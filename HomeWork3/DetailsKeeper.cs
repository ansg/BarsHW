﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HomeWork3
{
    public class DetailsKeeper
    {
        public List<string> EngineList;
        public List<string> BodyList;
        public List<string> WheelsList;
        public List<string> DoorsList;
        public List<string> EnginePowerList;

        public DetailsKeeper()
        {
            EngineList = File.ReadLines("Stock\\Engines.txt").ToList();
            BodyList = File.ReadLines("Stock\\CarBody.txt").ToList();
            WheelsList = File.ReadLines("Stock\\Wheels.txt").ToList();
            DoorsList = File.ReadLines("Stock\\Doors.txt").ToList();
            EnginePowerList = File.ReadLines("Stock\\EnginePower.txt").ToList();
        }

    }
}